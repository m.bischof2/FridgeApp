function doPost(e) {
  var json = JSON.parse(e.postData.contents);
  var result;

  switch (json.sheet) {
    case "fridgeData":
      console.log("handling fridgeData request: " + json.action);
      result = handleFridgeDataRequest(json.action, json.data);
      break;
    case "priceData":
      console.log("handling priceData request: " + json.action);
      result = handlePriceDataRequest(json.action, json.data);
      break;
    case "members":
      console.log("handling members request: " + json.action);
      result = handleMembersRequest(json.action, json.data);
      break;
    case "transferMembers":
      console.log("handling transferMembers request: " + json.action);
      result = handleTransferMembersRequest(json.action, json.data);
      break;
    case "classification":
      console.log("handling classification request: " + json.action);
      result = handleClassificationRequest(json.action, json.data);
      break;
  }

  return result;
}

function getSheet(sheetName) {
  // prod
  // var ss = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/14pgk4rdGZpXaMAmEtftmOsKIz-LRb-8zHVNaXWKlpIw/edit#gid=950053882");
  // dev
  var ss = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/13SNpQ0LGoGNdtB_1VtqEi6dFgjjlt0Poh3Gj7l-hgCA/edit#gid=950053882");
  return ss.getSheetByName(sheetName);
}

function handleFridgeDataRequest(action, data) {
  var sheet = getSheet("Kuehlhaus");

  if (action == "create") {
    // last row
    var lr= sheet.getLastRow();
    console.log(lr);
    var currentTime = Utilities.formatDate(new Date(), "GMT+1", "dd.MM.yyyy");// d.toLocaleString();

    var rowData = sheet.appendRow([currentTime, Utilities.formatDate(new Date(data.date), "GMT+2", "dd.MM.yyyy"), data.name, data.type, data.classification, data.location, data.age, data.weight, data.usage, data.transferMember, data.basePrice, data.price, false, false]);

    lr = sheet.getLastRow();
    // 13 is the first column with data validation
    var range = sheet.getRange(lr, 13, 1, 2);

    var enforceCheckbox = SpreadsheetApp.newDataValidation();
    enforceCheckbox.requireCheckbox();
    enforceCheckbox.setAllowInvalid(false);
    enforceCheckbox.build();

    range.setDataValidation(enforceCheckbox);

    console.log(rowData);
    console.log(data);

    sendMail(data);

    return ContentService.createTextOutput(JSON.stringify([])).setMimeType(ContentService.MimeType.JSON);
  } else if (action == "read") {
    var dataArray = [];

    // collecting data from 2nd Row , 1st column to last row and last column
    var rows = sheet.getRange(2,1,sheet.getLastRow()-1, sheet.getLastColumn()).getValues();

    for (var i = 0, l = rows.length; i < l; i++) {
      var dataRow = rows[i];
      var record = {};
      record['date'] = dataRow[1];
      record['name'] = dataRow[2];
      record['type'] = dataRow[3];
      record['classification'] = dataRow[4];
      record['location'] = dataRow[5];
      record['weight'] = dataRow[7];
      record['price'] = dataRow[11];
      record['confirmed'] = dataRow[12];
      record['paid'] = dataRow[13];

      dataArray.push(record);
    }

    var result = JSON.stringify(dataArray);
    console.log("entries found: " + dataArray.length);

    return ContentService.createTextOutput(result).setMimeType(ContentService.MimeType.JSON);
  }
}

    var result = JSON.stringify(dataArray);
    console.log("entries found: " + dataArray.length);

    return ContentService.createTextOutput(result).setMimeType(ContentService.MimeType.JSON);
  }
}

function handlePriceDataRequest(action, data) {
  var sheet = getSheet("Preisdaten");

  if (action == "read") {
    var dataArray = [];

    // collecting data from 2nd Row , 1st column to last row and last column
    var rows = sheet.getRange(2,1,sheet.getLastRow()-1, sheet.getLastColumn()).getValues();

    for(var i = 0, l= rows.length; i<l ; i++){
      var dataRow = rows[i];
      var record = {};
      record['type'] = dataRow[0];
      record['from'] = dataRow[1];
      record['to'] = dataRow[2];
      record['price'] = dataRow[3];

      dataArray.push(record);
    }

    var result = JSON.stringify(dataArray);
    console.log("entries found: " + dataArray.length);

    return ContentService.createTextOutput(result).setMimeType(ContentService.MimeType.JSON);
  }
}

function handleMembersRequest(action, data) {
  var sheet = getSheet("Mitgliederliste");

  if (action == "read") {
    var dataArray = [];

    // collecting data from 2nd Row , 1st column to last row and last column
    var rows = sheet.getRange(2,1,sheet.getLastRow()-1, sheet.getLastColumn()).getValues();

    for(var i = 0, l= rows.length; i<l ; i++){
      var dataRow = rows[i];
      var record = {};
      record['name'] = dataRow[0];

      dataArray.push(record);
    }

    var result = JSON.stringify(dataArray);
    console.log("entries found: " + dataArray.length);

    return ContentService.createTextOutput(result).setMimeType(ContentService.MimeType.JSON);
  }
}

function handleTransferMembersRequest(action, data) {
  var sheet = getSheet("Wildbret Weitergabe");

  if (action == "read") {
    var dataArray = [];

    // collecting data from 2nd Row , 1st column to last row and last column
    var rows = sheet.getRange(2,1,sheet.getLastRow()-1, sheet.getLastColumn()).getValues();

    for(var i = 0, l= rows.length; i<l ; i++){
      var dataRow = rows[i];
      var record = {};
      record['name'] = dataRow[0];

      dataArray.push(record);
    }

    var result = JSON.stringify(dataArray);
    console.log("entries found: " + dataArray.length);

    return ContentService.createTextOutput(result).setMimeType(ContentService.MimeType.JSON);
  }
}

function handleClassificationRequest(action, data) {
  var sheet = getSheet("Klassifizierungen");

  if (action == "read") {
    var dataArray = [];

    // collecting data from 2nd Row , 1st column to last row and last column
    var rows = sheet.getRange(2,1,sheet.getLastRow()-1, sheet.getLastColumn()).getValues();

    for(var i = 0, l= rows.length; i<l ; i++){
      var dataRow = rows[i];
      var record = {};
      record['type'] = dataRow[0];
      record['classification'] = dataRow[1];

      dataArray.push(record);
    }

    var result = JSON.stringify(dataArray);
    console.log("entries found: " + dataArray.length);

    return ContentService.createTextOutput(result).setMimeType(ContentService.MimeType.JSON);
  }
}

function sendMail(data) {
  var sheet = getSheet("Emails");

  var rows = sheet.getRange(2,1,sheet.getLastRow()-1, sheet.getLastColumn()).getValues();
  var recipient = '';

  for(var i = 0, l= rows.length; i<l ; i++){
    var dataRow = rows[i];
    recipient += dataRow[0];
    recipient += ',';
  }

  recipient.substring(0, recipient.lenght - 2);

  var subject = 'Neue Abschussmeldung für JG Möschach';
  var body = 'Eine neue Abschussmeldung wurde im Kühlhaus eingetragen!';
  body += '\n\nErlegungsdatum: ' + Utilities.formatDate(new Date(data.date), "GMT+2", "dd.MM.yyyy");
  body += '\nErleger: ' + data.name;
  body += '\nWildart: ' + data.type;
  body += '\nErlegungsort: ' + data.location;

  MailApp.sendEmail(recipient,subject, body);
}
