import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SheetService } from '../services/sheet.service';
import { Router } from '@angular/router';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import * as _moment from 'moment';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { initDomAdapter } from '@angular/platform-browser/src/browser';
import { DecimalPipe } from '@angular/common';
import { InputRestrictionService } from '../services/input-restriction.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD.MM.YYYY',
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-input',
  templateUrl: './input.page.html',
  styleUrls: ['./input.page.scss'],
  providers: [

    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class InputPage implements OnInit {

  _form: FormGroup;
  _priceData: any[];
  _members: string[];
  _transferMembers: string[];
  _types: string[];
  _price: number;
  _classifications: any[];

  constructor(private sheet: SheetService,
              private router: Router,
              private formBuilder: FormBuilder,
              private numberPipe: DecimalPipe,
              private inputRestriction: InputRestrictionService) {
    this._priceData = [];
    this._members = [];
    this._types = [];
    this._price = 0;
    this._classifications = [];
  }

  ngOnInit() {
    this.loadData();
    this.init();
  }

  loadData() {
    this.sheet.getPriceData().subscribe(data => {
      this._priceData = data;
      data.forEach(element => {
        if (this._types.indexOf(element.type) === -1) {
          this._types.push(element.type);
        }
      });
    });
    this.sheet.getMembers().subscribe(data => this._members = data);
    this.sheet.getTransferMembers().subscribe(data => this._transferMembers = data);
    this.sheet.getClassifications().subscribe(data => this._classifications = data);
  }

  doPostData() {
    this.sheet.postData(this._form.getRawValue()).subscribe(result => {
      this.inputRestriction.increaseCnt();
      this.router.navigate(['fridge']);
    });
  }

  ionViewWillEnter() {
    this.loadData();
    this.init();
  }

  init() {
    this._form = this.formBuilder.group({
      'date': new FormControl(null, Validators.required),
      'name': new FormControl(null, Validators.required),
      'type': new FormControl(null, Validators.required),
      'classification': new FormControl(null),
      'location': new FormControl(null, Validators.required),
      'age': new FormControl(null, Validators.required),
      'basePrice': new FormControl({value: null, disabled: true}, Validators.required),
      'weight': new FormControl(null, [Validators.required]),
      'price': new FormControl({value: null, disabled: true}, Validators.required),
      'usage': new FormControl(null, Validators.required),
      'transferMember': new FormControl(null)
    });

    this._form.get('type').valueChanges.subscribe(val => {
      this.calcPrice();

      if (this.getClassificationsForType(val).length > 0) {
        this._form.get('classification').setValidators(Validators.required);
      } else {
        this._form.get('classification').clearValidators();
        this._form.patchValue({ 'classification': null });
      }
    });
    this._form.get('weight').valueChanges.subscribe(val => this.calcPrice());
    this._form.get('usage').valueChanges.subscribe(val => {
      if (val === 'Weitergabe intern') {
        this._form.get('transferMember').setValidators(Validators.required);
      } else {
        this._form.get('transferMember').clearValidators();
      }
    });
  }

  calcPrice() {
    let type = this._form.get('type').value;
    let weight = this._form.get('weight').value;

    if (type && weight) {
      for (let pd of this._priceData) {
        if (pd.type === type && pd.from <= weight && weight < pd.to) {
          this._form.patchValue({ 'basePrice': this.numberPipe.transform(pd.price, '1.2-2')});
          this._form.patchValue({ 'price': this.numberPipe.transform((weight * pd.price), '1.2-2')});
          break;
        }
      }
    }
  }

  getClassificationsForType(type: string) {
    let filtered = this._classifications.filter(c => c.type === type);
    return filtered.map(c => c.classification);
  }
}
