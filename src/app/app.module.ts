import { NgModule, LOCALE_ID, Injectable, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MyMaterialModule } from './utils/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SheetService } from './services/sheet.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { LoadingInterceptor } from './interceptors/loading-interceptor';
import { registerLocaleData, DecimalPipe } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import { IonicStorageModule } from '@ionic/storage';

import * as Sentry from "@sentry/browser";

Sentry.init({
  dsn: "https://8af4288dbd344effba093ce440ac3fdb@sentry.io/1450321"
});

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  constructor() {}
  handleError(error) {
    if (environment.production) {
      const eventId = Sentry.captureException(error.originalError || error);
    }
    //Sentry.showReportDialog({ eventId });
  }
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    ReactiveFormsModule,
    FormsModule,
    MyMaterialModule,
    FlexLayoutModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    IonicStorageModule.forRoot({
      name: '__fridgeapp',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  providers: [
    DecimalPipe,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true },
    { provide: LOCALE_ID, useValue: "de-DE" },
    { provide: ErrorHandler, useClass: SentryErrorHandler },
    SheetService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    registerLocaleData(localeDe);
  }
}
