import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  { path: 'tabs', component: TabsPage, children: [
      { path: 'input', children: [
          { path: '', loadChildren: '../input/input.module#InputPageModule'}
        ]
      },
      { path: 'fridge', children: [
          { path: '', loadChildren: '../fridge/fridge.module#FridgePageModule' }
        ]
      },
      { path: '', redirectTo: '/tabs/input', pathMatch: 'full' }
    ]
  },
  { path: '', redirectTo: '/tabs/input', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
