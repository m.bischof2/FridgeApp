import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { UpdateService } from './services/update.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {
  constructor(
    private platform: Platform,
    private update: UpdateService) {
  }

  ngOnInit() {
    this.update.checkForUpdate();
  }
}
