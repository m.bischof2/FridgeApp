import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FridgePage } from './fridge.page';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MyMaterialModule } from '../utils/material.module';
import { DetailsComponent } from './details/details.component';

const routes: Routes = [
  {
    path: '',
    component: FridgePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MyMaterialModule,
    FlexLayoutModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    FridgePage,
    DetailsComponent
  ],
  entryComponents: [
    DetailsComponent
  ]
})
export class FridgePageModule {}
