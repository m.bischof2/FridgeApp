import { Component, OnInit } from '@angular/core';
import { SheetService } from '../services/sheet.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { InputRestrictionService } from '../services/input-restriction.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { DetailsComponent } from './details/details.component';

@Component({
  selector: 'app-fridge',
  templateUrl: './fridge.page.html',
  styleUrls: ['./fridge.page.scss'],
})
export class FridgePage implements OnInit {

  _fridgeData: Observable<any>;
  _displayColumns=['name', 'type', 'price', 'paid'];
  inputsToday: number;

  constructor(private _sheetService: SheetService,
              private router: Router,
              private inputRestriction: InputRestrictionService,
              private snackbar: MatSnackBar,
              public dialog: MatDialog) {
    this.inputRestriction.getTodayCnt().subscribe(cnt => this.inputsToday = cnt);
  }

  ngOnInit() {
    this._fridgeData = this._sheetService.getFridgeEntries();
  }

  ionViewWillEnter() {
    this._fridgeData = this._sheetService.getFridgeEntries();
  }

  newEntry() {
    if (this.inputsToday < 5) {
      this.router.navigate(['/input']);
    } else {
      this.snackbar.open('Du kannst nur 5 Einträge pro Tag machen!', null, { duration: 10000 });
    }
  }

  openDetails(row) {
    const dialogRef = this.dialog.open(DetailsComponent, {
      width: '800px',
      data: row
    });
  }

  reload() {
    this._fridgeData = this._sheetService.getFridgeEntries();
  }
}
