import { TestBed } from '@angular/core/testing';

import { InputRestrictionService } from './input-restriction.service';

describe('InputRestrictionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InputRestrictionService = TestBed.get(InputRestrictionService);
    expect(service).toBeTruthy();
  });
});
