import { Injectable } from '@angular/core';

import { MatSnackBar } from '@angular/material';
import { SwUpdate } from '@angular/service-worker';

@Injectable({
  providedIn: 'root'
})
export class UpdateService {
  constructor(private swUpdate: SwUpdate, private snackbar: MatSnackBar) {
  }

  checkForUpdate() {
    this.swUpdate.checkForUpdate();
    this.swUpdate.available.subscribe(evt => {
      const snack = this.snackbar.open('Es gibt ein Update, bitte lade die App neu!', 'App neu laden', { duration: 10000 });

      snack.onAction().subscribe(() => {
          window.location.reload();
      });
    });
  }
}
