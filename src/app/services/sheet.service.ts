import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SheetService {

  constructor(private http: HttpClient) {
  }

  getPriceData() : Observable<any> {
    let req : Request = {
        sheet: 'priceData',
        action: 'read'
    };
    return this.http.post(environment.sheetsURL, JSON.stringify(req));
  }

  getMembers() : Observable<any> {
    let req : Request = {
        sheet: 'members',
        action: 'read'
    };
    return this.http.post(environment.sheetsURL, JSON.stringify(req));
  }

  getTransferMembers() : Observable<any> {
    let req : Request = {
        sheet: 'transferMembers',
        action: 'read'
    };
    return this.http.post(environment.sheetsURL, JSON.stringify(req));
  }

  getClassifications() : Observable<any> {
    let req : Request = {
        sheet: 'classification',
        action: 'read'
    };
    return this.http.post(environment.sheetsURL, JSON.stringify(req));
  }

  postData(data) {
    let req : Request = {
        sheet: 'fridgeData',
        action: 'create',
        data: data
    };
    return this.http.post(environment.sheetsURL, JSON.stringify(req));
  }

  getFridgeEntries() : Observable<any> {
    let req : Request = {
      sheet: 'fridgeData',
      action: 'read'
    };

    return this.http.post(environment.sheetsURL, JSON.stringify(req));
  }
}

interface Request {
    sheet: string;
    action: string;
    data?: any;
}
