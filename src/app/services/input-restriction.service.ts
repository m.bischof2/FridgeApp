import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';

const STORAGE_KEY = 'todayCnt';

@Injectable({
  providedIn: 'root'
})
export class InputRestrictionService {

  private todayCnt: BehaviorSubject<number>;
  private currentCnt: number;
 
  constructor(private storage: Storage) {
    this.todayCnt = new BehaviorSubject<number>(0);

    this.storage.get(STORAGE_KEY).then(data => {
      if (data) {
        let entry = JSON.parse(data);

        if (this.isToday(new Date(entry.date))) {
          this.todayCnt.next(entry.count);
        } else {
          this.storage.set(STORAGE_KEY, JSON.stringify({ date: new Date(), count: 0 }));
          this.todayCnt.next(0);
        }
      } else {
        this.storage.set(STORAGE_KEY, JSON.stringify({ date: new Date(), count: 0 }));
        this.todayCnt.next(0);
      }
    });

    this.todayCnt.asObservable().subscribe(cnt => this.currentCnt = cnt);
  }

  private isToday(date) {
    const today = new Date()
    return date.getDate() === today.getDate() &&
            date.getMonth() === today.getMonth() &&
            date.getFullYear() === today.getFullYear();
  }

  getTodayCnt() {
    return this.todayCnt.asObservable();
  }

  increaseCnt() {
    this.todayCnt.next(this.currentCnt + 1);
    this.storage.set(STORAGE_KEY, JSON.stringify({ date: new Date(), count: this.currentCnt}));
  }
}
